///1. Опишіть, як можна створити новий HTML тег на сторінці.

///Є декілька шляхів для створення нових тегів: - за допомогою властивості innerHTML;
///- при створенні нового елемента document.createElement(tag).

///2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

///Дана функція розбирає вказаний текст як HTML чи XML і вставляє отримані вузли у DOM дерево у вказану позицію. Параметри: position, text.
///Position визначає позицію елементу, що додається, відносно елемента, що викликав метод (beforebegin, afterbegin, beforeend, afterend).

///3. Як можна видалити елемент зі сторінки?
///За допомогою метода Element.remove().


let firstArr = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  "1",
  "2",
  23,
];

const secondArr = document.createElement("div");
document.body.append(secondArr);

function addList(array, parent) {
  if (parent == undefined) {
    ul = document.createElement("ul");
    document.body.append(ul);

    array.forEach((element) => {
      li = document.createElement("li");
      ul.insertAdjacentHTML("beforebegin", `<li>${element}</li>`);
    });
  } else {
    array.forEach((element) => {
      li = document.createElement("li");

      parent.insertAdjacentHTML("afterbegin", `<li>${element}</li>`);
    });
  }
}

addList(firstArr, secondArr);

